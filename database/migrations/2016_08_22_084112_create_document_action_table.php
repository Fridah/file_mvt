<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentActionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_action', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('file_id')->unsigned();
              $table->string('doc_folio_number');
              $table->string('doc_heading');
              $table->string('category');
             $table->string('received_from');
             $table->string('action_to_be_done');
             $table->integer('responsible_user');
             $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('document_action');
    }
}

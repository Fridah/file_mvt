<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Notification;
use App\Document_action;
use App\MailOut;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $notifications = Document_action::where( 'status', '0')
                        ->whereRaw('hour(timediff(now(), created_at)) >= 24')->get();

        //echo "Total Document Actions ".$notifications->count()."\n";

        foreach ($notifications as $n) {

           //echo $n->responsible->email." ";
  //echo $n->responsible->fname." ";
           try{
                $notificationData = ['subject'=>'Request',
                             'body'=>'There is a file overdue, please attend to it.',
                             'recipientMail'=>$n->responsible->email,
                             'recipientName'=>$n->responsible->fname, 
                             'cc'=>''
                             ];
                
                

                $mailOut = MailOut::create($notificationData);

                //print_r($notificationData);

            }catch(\Exception $ex){
               //echo $ex;    
            }                 
         
           // echo "\n";
            //Notification::send_notification();
        }

        

        $mailouts = MailOut::where('status', 0)->get();

         foreach($mailouts as $mailout){
             $mailArray = $mailout->toArray();
             Notification::send_notification($mailArray);
             $mailout->status = 1;
             $mailout->save();


         }

    }
}

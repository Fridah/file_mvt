<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File_action extends LogModel
{
    //
    protected $table='file_action';

    protected $fillable =[

       'received_from',
       'action_to_be_done',
       'responsible_director',
       'comments'
        ];

	public function getSender()
	{
		$user_id = $this->received_from;

		$user = User::find($user_id);

		if($user == null){
			return "-";
		}

		if($user->role == "director"){
			return $user->directorate;
		}else{
			return $user->role;
		}
	}

}
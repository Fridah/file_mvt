<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Route;
use DB;
class Document_action extends LogModel
{
   protected $table='document_action';

    protected $fillable =[

       'doc_folio_number',
       'doc_heading',
       'category',
       'received_from',
       'action_to_be_done',
       'responsible_user',
       'status'
        ];


   public function request_from()
    {
        return $this->belongsTo('App\User', 'received_from');
    }

	 public function responsible()
    {
        return $this->belongsTo('App\User', 'responsible_user');
    }

    public static function getCategory() // helps to get data for reports
    {
      $getCategory = DB::table('document_action')
                    ->select(DB::raw('count(*) as total, category, users.fname, users.mname, users.lname '))
                    ->join('users', 'users.id','=', 'document_action.responsible_user')
                    ->groupBy('category')
                    ->groupBy('responsible_user')
                    ->groupBy(DB::raw('week(document_action.created_at)'))
                    ->get();
        return $getCategory;
    }
}

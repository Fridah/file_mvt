<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends LogModel
{
     use SoftDeletes;
	 public function documents(){

	 	return $this->hasMany('Document', 'file_id');
	 }
     // $dd = date('y-m-d', strtotime('$file->date_created'));
     // File::query()->date_created = $dd;
    protected $fillable = [
    	'file_reference_number',
    	'date_created',
    	'file_name',
        'index_heading',
    	'closed_date',
    	'status'
    ];
    protected $dates = ['deleted_at'];

}

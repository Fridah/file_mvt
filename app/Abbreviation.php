<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abbreviation extends Model
{
    protected $fillable =[
    	'abbreviation'
    ];

}

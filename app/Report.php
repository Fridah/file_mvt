<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable =[
    	'name',
    	'category',
    	'number_of_documents_attended',
    	'total'
    ];

}

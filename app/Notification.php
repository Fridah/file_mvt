<?php

namespace App;
use Mail;

class Notification
{
   public static function send_notification($mailData=[])
   {
   	$data=[];
   	 Mail::send('emails.send_notification', ['data'=>$mailData], function ($message) use($mailData){
     $message->from('fridahfulgence@gmail.com', 'FMS');

    $message->to($mailData['recipientMail'])->subject('Requests');

    }); 

   }
}

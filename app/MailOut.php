<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailOut extends Model
{
    protected $table='mailOut';

    protected $fillable = [
    'subject',
    'body',
    'cc',
    'recipientMail',
    'recipientName',
    'status'
    ];
}

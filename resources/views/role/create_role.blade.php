@extends('dashboard_app')

@section('content')
	 {!!Form::open(['url'=>route('role.store')]) !!}
	
	@include('role.role_form',['submitButtonText'=>'Create'])
	 
	 {!!Form::close() !!}

     @include('errors.list')
@stop
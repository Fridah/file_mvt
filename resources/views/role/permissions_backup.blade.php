 @extends('new_layout')
@section('content')
	<div id="viewfiles-wrap">
	<form action="{{url('role/permissions', $role->id)}}" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	 <div class="card-box table-responsive">
	 	<table>	
			<tbody>
		        @foreach($permissions as $permission)
					<tr>
			            <td>

			            <input type="checkbox" name="permission[]" value="{{$permission->id}}" 	{{($role->perms->contains('id',$permission->id))?'checked="checked"':""}} 
			            
			            />
							<!-- (condition)?statement1:statement2 ....ternary statement-->
			            
			            </td>
			            <td>{{$permission->display_name}}</td>
			        </tr>
		        @endforeach
			      <!-- end foreach -->
			</tbody>
			</table>
    	</div>
    	
	    	 <button type="submit" class="btn btn-deep-green" id="btn-login" class="btn btn-primary form-control">Save</button>
    	 </form>
    </div>
@stop


@extends('new_layout')

@section('content')
	<div class="x-panel">
		<div id="viewfiles-wrap">
			<div class="x-content">
				<div class="row">
					<div class="col-sm-12">
						<div class="card-box table-responsive">	
							<table id="table1" class="table table-striped table-bordered">
							<thead>
								<th>No.</th>
								<th>User Id</th>
								<th>Model</th>
								<th>Action</th>
								<th>Content</th>
							</thead>
							<tbody>
							@foreach($logs as $log)
								<tr>
									<td>{{$log->id}}</td>
									<td>{{$log->user_id}}</td>
									<td>{{$log->model}}</td>
									<td>{{$log->action}}</td>
									<td>{{$log->content}}</td>
								</tr>
							@endforeach
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop
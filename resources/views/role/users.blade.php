@extends('dashboard_app')

@section('content')
	<h3><i class="fa fa-users" aria-hidden="true"></i> Users</h3>
	<hr />
	<div class="card-box table-responsive">	
		<table id="table1" class="table table-striped table-bordered">
			<thead>
				<th>No.</th>
				<th>First Name</th>
				<th>Middle Name</th>
				<th>Last Name</th>
				<th>Position</th>
				<th>Abbreviation</th>
				<th>Mobile number</th>
				<th>Email</th>
				<th>Action</th>
			</thead>
			<tbody>
			@foreach($users as $user)
				<tr>
					<td>{{$user->id}}</td>
					<td>{{$user->fname}}</td>
					<td>{{$user->mname}}</td>
					<td>{{$user->lname}}</td>
					<td>{{$user->position}}</td>
					<td>{{$user->abbreviation}}</td>
					<td>{{$user->mobile_no}}</td>
					<td>{{$user->email}}</td>
					<td>
						<a class="btn btn-sm btn-primary" href="{{route('role.assign_role', $user->id)}}"><i class="fa fa-key"></i>
		            </a>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div> <!-- end card-box table-responsive -->
@stop
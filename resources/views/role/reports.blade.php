@extends('dashboard_app')

@section('content')
	<h3><i class="fa fa-bar-chart-o" aria-hidden="true"></i> General Report</h3>
	<hr />
	<div class="card-box table-responsive">	
		<table id="table1" class="table table-striped table-bordered">
			<thead>
				<th>Name</th>
				<th>Type of Document</th>
				<th>Number of Documents attended per week</th>
				<th>Total</th>
			</thead>
			<tbody>
				@foreach($reports as $report)
				<tr>
					<td>{{$report->fname}} {{$report->mname}} {{$report->lname}}</td>
					<td>{{$report->category}}</td>
					<td>{{$report->total}}</td>
					<td> </td>
				</tr>

				@endforeach
			</tbody>
		</table>
	</div> <!-- end card-box table-responsive -->




@stop
<div id="doc_info">
	<div class="row">
    	<div class="col-md-offset-2 col-md-8 col-md-offset-2">
        	<div class="card">
            	<div class="card-block">
            		<!--Header-->
                <div class="text-xs-center form-heading">
                    <h3><i class="fa fa-comments"></i> Request for Action:</h3>
                    <hr class="m-t-2 m-b-2">
                </div>

                <br>
                <!--Body-->
                <div class="form-group">
          	 	
        				 	{!!Form::label('action_needed','Action') !!}
        				 	{!!Form::textarea('action_to_be_done',null, [' class'=> 'form_control'])!!}
        					
        				</div>
        				<div id="request-dropdown">
                  <div class="chosen-position">
                  {!!Form::select('responsible_director',$abbreviation,null,['class'=>'chosen-select chosen', 'style'=>'width:200px;'])!!}
                  </div>
                </div>
                
                <div class="form-group">
      					{!! Form::submit('Request',['id'=>'submit-btn','class'=> 'btn btn-primary form-control']) !!}
      				 </div>
				 	
	 	
            	</div>
        	</div>
    	</div>
	</div>
</div>
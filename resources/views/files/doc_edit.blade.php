@extends('new_layout')

@section('content')
	 {!!Form::model($document, array('route' => array('files.documents.doc_update', $document->id),'method'=>'POST')) !!}
	
	@include('files.doc_form',['submitButtonText'=>'Save'])
	 
	 {!!Form::close() !!}


		@include('errors.list')

@stop





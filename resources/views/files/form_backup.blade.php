<div id="form-wrap">
<h1>File Information</h1>
	<div class="form-group">
	 	
	 	{!!Form::label('file_reference_number','File reference number') !!}
	 	{!!Form::text('file_reference_number',null, ['class'=> 'form-control'])!!}
		
	 </div>

<div class="form-group">
	 	
	 	{!!Form::label('date_created','date created') !!}
	 	{!!Form::text('date_created',null, ['class'=> 'form-datetime'])!!}
		
	 </div>

<div class="form-group">
	 	
	 	{!!Form::label('file_description','file description') !!}
	 	{!!Form::text('file_description',null, ['class'=> 'form-control'])!!}
		
	 </div>

<div class="form-group">
	 	
	 	{!!Form::label('letter_folio_number','folio/minute number') !!}
	 	{!!Form::text('letter_folio_number',null, ['class'=> 'form-control'])!!}
		
	 </div>
<div class="form-group">
	 	
	 	{!!Form::label('letter_ref_number','letter reference number') !!}
	 	{!!Form::text('letter_ref_number',null, ['class'=> 'form-control'])!!}
		
	 </div>
<div class="form-group">
	 	
	 	{!!Form::label('letter_written_date','letter written date') !!}
	 	{!!Form::text('letter_written_date',null, [' class'=> 'form_datetime'])!!}
		
	 </div>

<div class="form-group">
	 	
	 	{!!Form::label('received_date','received date') !!}
	 	{!!Form::text('received_date',null, [' class'=> 'form_datetime'])!!}
		
	 </div>




	 	
	 	 <div id="request-dropdown">
	 	<h3>Responsible Director</h3> <b>:</b>
					<select name="directorate">
					<option value="-"> </option>
					<option value="DCIO">DCIO</option>
					<option value="DBS">DBS</option>
					<option value="DESC">DESC</option>
					<option value="DIMS">DIMS</option>
					</select>

	 </div>
		



<div class="form-group">
	 	
		{!!Form::submit($submitButtonText,['class'=> 'btn btn-primary form-control'])!!}
		
	 </div>
</div>	


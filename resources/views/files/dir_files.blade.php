@extends('new_layout')

@section('content')
	<div class="x-panel">
		<div id="viewfiles-wrap">
			<h3><i class="fa fa-file-text" aria-hidden="true"></i> Documents</h3>
			<hr>
				<div class="x-content">
					<div class="row">
						<div class="col-sm-12">
							<div class="card-box table-responsive">		
								<table id="table1" class="table table-striped table-bordered">
								<thead>
								<th width="4%">No.</th>
								<th width="3%">File Reference Number</th>
								<th width="6%">Date created</th>
								<th>File Name</th>
								<th width="3%">Document Reference Number</th>
								<th width="3%">Folio/minute Number</th>
								<th>Document written Date</th>
								<th width="6%">Received Date</th>
								<th>directorate</th>
								<th width="6%">Closed Date</th>
								<th width="13%">Action</th>
								</thead>
						<tbody>

						@foreach($dir_files as $file)
						<tr>
						<td>{{$file->id}}</td>

						<td>{{$file->file_reference_number}}</td>

						<td>{{$file->date_created}}</td>
						<td>{{$file->file_description}}</td>
						<td>{{$file->letter_folio_number}}</td>
						<td>{{$file->letter_ref_number}}</td>
						<td>{{$file->letter_written_date}}</td>
						<td>{{$file->received_date}}</td>
						<td>{{$file->directorate}}</td>
						<td>{{$file->closed_date}}</td>
						<td>
							<a class="btn btn-sm btn-default" href="{{route('files.filerequest.create', $file->id)}}"><i class="fa fa-comments"></i>
	                		</a>
	                	</td>
						<!-- <a href="{{route('files.filerequest.create', $file->id)}}">request</a> </td> -->
						</tr>
										
						@endforeach
						</tbody>
							</table>
						</div>
					</div>
				</div>
		</div>
	</div>
@stop
@extends('app')
@section('content')
	<h2>Your Registration is Complete</h2>
	<img src="{{asset('images/okay.png')}}" alt="#">
	<a class=" btn-link" href="{{ url('auth/login') }}">Continue</a>
@endsection

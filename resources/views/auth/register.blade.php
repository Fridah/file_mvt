@extends('app')

@section('content')
<!--Form with header-->
<div class="row">
    <div class="col-md-offset- 3 col-md-6 col-md-offset-3">
        <div class="card z-depth-3">
    <div class="card-block">
    @if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>Whoops!</strong> There were some problems with your input.<br><br>
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
        <form class="form-horizontal login" role="form" method="POST" action="{{ url('/auth/register') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <!--Header-->
        <div class="form-header teal darken-3-gradient">
            <h3><i class="fa fa-user"></i> Register:</h3>
        </div>
            
        <div class="md-form">
            <!-- <i class="fa fa-user prefix"></i> -->
            <input type="text" id="form3" class="form-control" name="fname" value="{{ old('fname') }}" placeholder="Your First name">
        </div>
        <div class="md-form">
            <!-- <i class="fa fa-user prefix"></i> -->
            <input type="text" id="form3" class="form-control" name="mname" value="{{ old('mname') }}" placeholder="Your Middle name">
        </div>
        <div class="md-form">
            <!-- <i class="fa fa-user prefix"></i> -->
            <input type="text" id="form3" class="form-control" name="lname" value="{{ old('lname') }}" placeholder="Your Last name">
        </div>
        <div class="md-form">
            <!-- <i class="fa fa-envelope prefix"></i> -->
            <input type="text" id="form2" class="form-control" name="position" value="{{ old('position') }}" placeholder="Your Position">
            <!-- <label for="form2">Your email</label> -->
        </div>
        <div class="md-form">
            <!-- <i class="fa fa-envelope prefix"></i> -->
            <input type="text" id="form2" class="form-control" name="abbreviation" value="{{ old('abbreviation') }}" placeholder="Your Abbreviation">
            <!-- <label for="form2">Your email</label> -->
        </div>
      
        <div class="md-form">
            <!-- <i class="fa fa-phone prefix"></i> -->
            <input type="text" id="form2" class="form-control" name="mobile_no" value="{{ old('mobile_no') }}" placeholder="Your Mobile Number">
            <!-- <label for="form2">Your email</label> -->
        </div>
        <div class="md-form">
            <!-- <i class="fa fa-envelope prefix"></i> -->
            <input type="text" id="form2" class="form-control" name="email" value="{{ old('email') }}" placeholder="Your Email">
            <!-- <label for="form2">Your email</label> -->
        </div>

        <div class="md-form">
            <!-- <i class="fa fa-lock prefix"></i> -->
            <input type="password" id="form4" class="form-control" name="password" placeholder="Your Password">
            <!-- <label for="form4">Your password</label> -->
        </div>
        <div class="md-form">
            <!-- <i class="fa fa-lock prefix"></i> -->
            <input type="password" id="form4" class="form-control" name="password_confirmation" placeholder="Confirm your Password">
            <!-- <label for="form4">Your password</label> -->
        </div>

        <div class="text-xs-center">
            <button type="submit" class="btn btn-green" id="btn-reg"> Sign up</button>
        </div>
      </form>
    </div>
</div>
<!--/Form with header-->
    </div>
</div>
@endsection
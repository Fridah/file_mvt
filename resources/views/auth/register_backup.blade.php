@extends('app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Register</div>
        <div class="panel-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>Whoops!</strong> There were some problems with your input.<br><br>
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div id="role-dropdown">
            
          <h3>Role</h3> <b>:</b>
          <select name="role">
          <option value="">select role</option>
          <option value="director">Director</option>
          <option value="entry">Data Entry</option>
          </select>
          </div>



          <div id="directorate-dropdown">
            
          <h3>Directorate</h3> <b>:</b>
          <select name="directorate">
          <option value="-"> </option>
          <option value="DCIO">DCIO</option>
          <option value="DBS">DBS</option>
          <option value="DESC">DESC</option>
          <option value="DIMS">DIMS</option>
          </select>
          </div>

            <div class="form-group">
              <label class="col-md-4 control-label">username</label>
              <div class="col-md-6">
                <input type="text" class="form-control" name="username" value="{{ old('name') }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">E-Mail Address</label>
              <div class="col-md-6">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Password</label>
              <div class="col-md-6">
                <input type="password" class="form-control" name="password">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Confirm Password</label>
              <div class="col-md-6">
                <input type="password" class="form-control" name="password_confirmation">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                  Register
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div><div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Register</div>
        <div class="panel-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>Whoops!</strong> There were some problems with your input.<br><br>
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div id="role-dropdown">
            
          <h3>Role</h3> <b>:</b>
          <select name="role">
          <option value="">select role</option>
          <option value="director">Director</option>
          <option value="entry">Data Entry</option>
          </select>
          </div>



          <div id="directorate-dropdown">
            
          <h3>Directorate</h3> <b>:</b>
          <select name="directorate">
          <option value="-"> </option>
          <option value="DCIO">DCIO</option>
          <option value="DBS">DBS</option>
          <option value="DESC">DESC</option>
          <option value="DIMS">DIMS</option>
          </select>
          </div>

            <div class="form-group">
              <label class="col-md-4 control-label">username</label>
              <div class="col-md-6">
                <input type="text" class="form-control" name="username" value="{{ old('name') }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">E-Mail Address</label>
              <div class="col-md-6">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Password</label>
              <div class="col-md-6">
                <input type="password" class="form-control" name="password">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Confirm Password</label>
              <div class="col-md-6">
                <input type="password" class="form-control" name="password_confirmation">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                  Register
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

</div>
@endsection

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>FMS-admin</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="{{ URL::to('css/bootstrap.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/mdb.min.css ') }}">
     <!-- FONTAWESOME STYLES-->
    <link rel="stylesheet" href="{{ URL::to('css/font-awesome.min.css ') }}">
        <!-- CUSTOM STYLES-->
    <link href="{{ URL::to('css/custom.css')}}" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   <!-- DATATABLES CSS-->
   <link href="{{ URL::to('js/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="../images/logo.png">
</head>
<body>
	
	<div id="wrapper">
		<div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="{{URL::to('images/logo.png')}}" />
                    </a>
                </div>
              
                 <span class="logout-spn" >
                    
                    @if (Auth::user() != null)
                        <a href="{{url('auth/logout')}}">
                        <i class="fa fa-sign-out"></i> Logout</a> 
                    @endif
                
                </span>
            </div>
        </div> <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li >
                        <a href="{{url('role')}}" ><i class="fa fa-user "></i>Roles </a>
                    </li>
                    <li>
                        <a href="{{url('logs')}}"><i class="fa fa-sliders "></i>Logs </a>
                    </li>
                    <li>
                        <a href="{{url('users')}}"><i class="fa fa-users "></i>Users </a>
                    </li>
                    <li>
                        <a href="{{url('reports')}}"><i class="fa fa-bar-chart-o"></i>Reports</a>
                    </li>
                </ul>
              </div>
        </nav> <!-- /. NAV SIDE  -->

        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                      @yield('content')
                    </div>
                </div> <!-- /. ROW  -->             
                 
                  <hr />          
            </div><!-- /. PAGE INNER  -->
             
        </div> <!-- /. PAGE WRAPPER  -->
        <div class="footer">
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 eGovernment Agency | Design by: <a href="#" style="color:#fff;"  target="_blank">ega.go.tz</a>
                </div>
        </div>
        </div>

		
	</div> <!-- /. MAIN WRAPPER  -->
	
    <script src="{{ URL::to('js/mdb.min.js') }}"></script>	
    <script src="{{ URL::to('js/jquery.min.js') }}"></script>
	<script src="{{ URL::to('js/bootstrap.min.js') }}"></script>	
	<script src="{{ URL::to('js/custom.js') }}"></script>	
    <script src="{{ URL::to('js/datatables/jquery.dataTables.min.js') }}"></script> 
	<script src="{{ URL::to('js/laravel.js') }}"></script>	
	<script src="{{ URL::to('js/chosen.jquery.js') }}"></script>		
	<script>
		$(document).ready(function(){
			$('#table1').DataTable();
		});
	</script>

</body>
</html>